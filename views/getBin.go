package views

import (
	"errors"
	"github.com/labstack/echo/v4"
	"gitlab.com/iamtkadir/catbin/db"
	"gitlab.com/iamtkadir/catbin/models"
	"gorm.io/gorm"
	"net/http"
)

func GetBin(c echo.Context) error {

	id := c.Param("id")

	if id == "" {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"error": "Wrong URL",
		})
	}

	var paste models.PasteModel

	result := db.DB.First(&paste, "id = $1", id)
	// check error ErrRecordNotFound
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {

		return c.JSON(http.StatusBadRequest, echo.Map{"error": "Record not found"})
	}

	return c.JSON(http.StatusOK, paste)

}
