package views

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/iamtkadir/catbin/db"
	"gitlab.com/iamtkadir/catbin/models"
	"net/http"
	"time"
)

func CreateBin(c echo.Context) error {
	link := "localhost:8000"
	fmt.Println("Link", link)
	uuId := uuid.New().String()
	main_uuid := uuId[:8]

	var paste models.ReceiverPasteModel
	now := time.Now().Format(time.Stamp)

	err := c.Bind(&paste)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, echo.Map{
			"error": "Unprocessable",
		})
	}
	//creating paste adding uuid and timestamp
	db.DB.Create(&models.PasteModel{Id: main_uuid, Title: paste.Title, Content: paste.Content, CreatedAt: now})
	//returning paste url
	url := fmt.Sprintf("%s/%s", link, main_uuid)
	return c.JSON(http.StatusCreated, echo.Map{
		"link": url,
	})

}
