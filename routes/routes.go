package routes

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/iamtkadir/catbin/views"
)

func GetRoutes(e *echo.Echo) {
	e.GET("/:id", views.GetBin)
	e.POST("/", views.CreateBin)
}
