package models

type PasteModel struct {
	Id        string `json:"id"`
	Title     string `json:"title"`
	Content   string `json:"content"`
	CreatedAt string `json:"created_at"`
}
type ReceiverPasteModel struct {
	Title   string `json:"title"`
	Content string `json:"content"`
}
