package db

import (
	"gitlab.com/iamtkadir/catbin/models"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"log"
)

var DB *gorm.DB

func Connect(){
	db , err := gorm.Open(sqlite.Open("new.sqlite3"), &gorm.Config{})
	if err != nil{
		log.Fatal("Error at " , err)
	}
	log.Println("DB connected")

	DB = db
}

func Migrate (){
	err := DB.AutoMigrate(&models.PasteModel{})
	if err != nil {
		log.Fatal("Error in Migrating")
	}
}