package main

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/iamtkadir/catbin/db"
	"gitlab.com/iamtkadir/catbin/routes"
)

func main() {
	// Echo instance
	e := echo.New()
	//DB
	db.Connect()
	db.Migrate()

	// Routes
	routes.GetRoutes(e)

	// Start server
	e.Logger.Fatal(e.Start("127.0.0.1:8000"))
}
